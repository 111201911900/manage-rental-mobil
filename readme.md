# Binar Challange Cp5 
# Halaman Admin Rental Mobil
di buat oleh sayyida Amira Muthia Dina

## cara menjalankan

1. instal semua dependensi yang di butuhkan mengunakan perintah di bawah

```properties
    yarn install
```

2. buat database mengunakan perintah di bawah

```properties
    yarn sequelize db:create
```
3. ubah password dan database pada file [config](config/config.json)

4. Migrate semua data ke database mengunakan perintah di bawah

```properties
    yarn sequelize db:migrate
```

5. jalankan server dengan perintah di bawah

```properties
    yarn dev
```
6. tampilan awal yang akan muncul setelah di run
![tampilan](public/img/tampilan_awal.png)

7. setelah menambahkan data maka akan muncul list mobil
![tampilan](public/img/setelah_add.png)



## ERD Database 


![erd](public/img/ERD_db.png)


## API

### List API

menampilakan data dalam database mengunakan link `/api/cars`. untuk melihat code [klik disini](app/api/controllers/controller.js) pada line 3.


### Get API

untuk menampikan data mengunakan id dapat mengunakan link `/api/car/:id`.untuk melihat code terdapat pada line 74 [klik disini](app/api/controllers/controller.js).


### Create API

Cara menggunakan api ini. Badan permintaan harus berisi name, price, size, picture. Contohnya bisa dilihat di bawah ini. Anda dapat mengakses api di url `/api/car` ini dengan metode POST.
untuk melihat code [klik disini](app/api/controllers/controller.js) pada line 12. 


### Delete API

untuk melihat code [klik disini](app/api/controllers/controller.js) pada line 63. berfungsi dalam metode GET, urlnya adalah `/api/delete/:id`


### Update API

untuk melihat code [klik disini](app/api/controllers/controller.js) pada line 84. untuk mengupdate data melalui id dapat mengunakan url `/api/update/:id` dengan metode POST.



### Filter API

untuk melihat code [klik disini](app/api/controllers/controller.js) pada line 134. filter untuk  memisahkan data mobil yang sesuai dengan ukuran yang diberikan di url.

